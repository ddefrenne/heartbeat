use chrono::{Duration, Local, SecondsFormat};
use clap::{load_yaml, App};
use std::fs::File;
use std::io::stdout;
use std::io::Stdout;
use std::io::Write;

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    let url = match matches.value_of("url") {
        Some(url) => {
            println!("Starting heartbeat-check for {}", url);
            url
        }
        None => {
            eprintln!("No URL given.");
            std::process::exit(1);
        }
    };

    let mut out = Output::Stdout(stdout());

    if let Some(path) = matches.value_of("file") {
        out = Output::File(
            std::fs::OpenOptions::new()
                .write(true)
                .append(true)
                .create(true)
                .open(path)
                .unwrap_or_else(|err| {
                    eprintln!("Logfile could not be opened: {}", err);
                    std::process::exit(1);
                }),
        );
    };

    let interval = match matches.value_of("interval").unwrap().parse::<i64>() {
        Ok(seconds) => seconds,
        Err(_) => {
            eprintln!("Interval requires a number");
            std::process::exit(1);
        }
    };

    let only_server_errors = matches.occurrences_of("only_server_errors") > 0;

    let mut message = None;

    loop {
        let timestamp = Local::now();

        let elapsed = Duration::span(|| {
            match reqwest::blocking::get(url) {
                Ok(response) => {
                    if response.status().is_server_error() {
                        message = Some(format!("{}", response.status().as_str()));
                    } else if !only_server_errors {
                        message = Some(format!("{}", response.status().as_str()));
                    }
                }
                Err(error) => {
                    if error.is_redirect() {
                        message = Some(String::from("Timeout error"));
                    } else if error.is_timeout() {
                        message = Some(String::from("Too many redirects"));
                    } else {
                        message = Some(format!("error: {}", error));
                    }
                }
            };
        });

        if let Some(ref msg) = message {
            let duration;

            if elapsed.num_seconds() == 0 && elapsed.num_milliseconds() == 0 {
                duration = format!("{}μs", elapsed.num_microseconds().unwrap());
            } else if elapsed.num_seconds() == 0 {
                duration = format!("{}ms", elapsed.num_milliseconds());
            } else if elapsed.num_seconds() > 5 {
                duration = format!("{}s", elapsed.num_seconds(),);
            } else {
                duration = format!(
                    "{}s {}ms",
                    elapsed.num_seconds(),
                    elapsed.num_milliseconds()
                );
            }

            let line = format!(
                "[{}]: {} ({})",
                timestamp.to_rfc3339_opts(SecondsFormat::Secs, true),
                msg,
                duration
            );

            writeln!(out, "{}", line).unwrap_or_else(|err| {
                eprintln!("Failed to write to output: {}", err);
                std::process::exit(1);
            });
        }

        std::thread::sleep(Duration::seconds(interval).to_std().unwrap());
    }
}

enum Output {
    Stdout(Stdout),
    File(File),
}

impl Write for Output {
    fn flush(&mut self) -> Result<(), std::io::Error> {
        match self {
            Output::Stdout(out) => out.flush(),
            Output::File(file) => file.flush(),
        }
    }

    fn write(&mut self, bytes: &[u8]) -> Result<usize, std::io::Error> {
        match self {
            Output::Stdout(out) => out.write(bytes),
            Output::File(file) => file.write(bytes),
        }
    }
}
