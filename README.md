# Heartbeat

Performs a HTTP heartbeat-check every 5 seconds on a given URL.

# Help
```bash
USAGE:
    hb [FLAGS] [OPTIONS] <URL>

FLAGS:
    -h, --help                  Prints help information
        --only-server-errors    Only log HTTP statuscode 5xx
    -V, --version               Prints version information

OPTIONS:
    -f, --file <FILE>            A filepath to write the output to, the default is stdout
    -i, --interval <INTERVAL>    How many seconds between each check [default: 5]

ARGS:
    <URL>    The URL to check
```

# Build for Linux from MacOS
Uses https://github.com/rust-embedded/cross.

```bash
$ cross build --release --target x86_64-unknown-linux-gnu
```
